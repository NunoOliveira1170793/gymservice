package pt.isep.dao;

import pt.isep.model.*;

import java.io.*;

public class FileOperation {

    private static final String FILE_NAME = "GymService.dat";

    public static GymCompany loadGymCompany(){

        GymCompany gymCompany = null;

        try {
            File file = new File(FILE_NAME);

            if (!file.exists()) {

                gymCompany = new GymCompany();

                User user1 = new User("Paulo", "2750Az");
                User user2 = new User("Andreia", "2310Ae");
                User user3 = new User("Nuno", "67aBzo");
                User user4 = new User("Juliana", "178Up2");

                gymCompany.addUser(user1);
                gymCompany.addUser(user2);
                gymCompany.addUser(user3);
                gymCompany.addUser(user4);

                ClassSession session1 = new ClassSession(10,11, ClassSession.WeekDay.MONDAY);
                ClassSession session2 = new ClassSession(16,17, ClassSession.WeekDay.WEDNESDAY);
                ClassSession session3 = new ClassSession(20,21, ClassSession.WeekDay.SATURDAY);
                ClassSession session4 = new ClassSession(14,15, ClassSession.WeekDay.THURSDAY);

                Schedule schedule1 = new Schedule();
                schedule1.addClassSession(session1);
                schedule1.addClassSession(session2);
                schedule1.addClassSession(session3);

                Schedule schedule2 = new Schedule();
                schedule2.addClassSession(session3);
                schedule2.addClassSession(session4);

                GymClass gymClass1 = new GymClass("Body Pump", schedule1);
                gymClass1.addAttendee(user1);
                gymClass1.addAttendee(user2);
                gymClass1.addAttendee(user4);
                GymClass gymClass2 = new GymClass("Yoga", schedule2);
                gymClass2.addAttendee(user3);
                gymClass2.addAttendee(user4);
                gymClass2.addAttendee(user2);

                gymCompany.addGymClass(gymClass1);
                gymCompany.addGymClass(gymClass2);

                storeGymCompany(gymCompany);
            }else{

                ObjectInputStream ois = null;
                FileInputStream fis = null;

                try{
                    fis = new FileInputStream(file);
                    ois = new ObjectInputStream(fis);
                    gymCompany = (GymCompany) ois.readObject();
                } catch (EOFException e){
                    e.printStackTrace();
                }finally{
                    fis.close();
                    ois.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return gymCompany;
    }


    public static void storeGymCompany(GymCompany gymCompany){
        try {
            File file = new File(FILE_NAME);
            FileOutputStream fos;
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(gymCompany);
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
