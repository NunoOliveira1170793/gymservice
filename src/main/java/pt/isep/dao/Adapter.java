package pt.isep.dao;

import pt.isep.model.GymClass;
import pt.isep.model.GymCompany;
import pt.isep.model.Schedule;
import pt.isep.model.User;

import java.util.ArrayList;

public class Adapter {

    /*********************************************************/
    //Global

    public static GymCompany getGymCompany(){
        GymCompany gymCompany = FileOperation.loadGymCompany();
        return gymCompany;
    }

    /*********************************************************/
    //Users

    public static ArrayList<User> getUsers(){
        GymCompany gymCompany = FileOperation.loadGymCompany();
        if(gymCompany != null) {
            return gymCompany.getUsers();
        }
        return null;
    }

    public static User getUser(int id){
        GymCompany gymCompany = FileOperation.loadGymCompany();

        if(gymCompany != null) {
            return gymCompany.findUser(id);
        }
        return null;
    }

    public static boolean addUser(User user){
        GymCompany gymCompany = FileOperation.loadGymCompany();
        gymCompany.addUser(user);
        FileOperation.storeGymCompany(gymCompany);
        return true;
    }

    public static  boolean updateUser(int id, User u){
        boolean ret = false;
        GymCompany gymCompany = FileOperation.loadGymCompany();
        User user = gymCompany.updateUser(id, u);
        if(user != null) {
            FileOperation.storeGymCompany(gymCompany);
            ret = true;
        }
        return ret;
    }

    public static  boolean deleteUser(int id){
        boolean ret = false;
        GymCompany gymCompany = FileOperation.loadGymCompany();
        User user = gymCompany.deleteUser(id);
        if(user != null) {
            FileOperation.storeGymCompany(gymCompany);
            ret = true;
        }
        return ret;
    }

    /*********************************************************/
    //GymClasses

    public static ArrayList<GymClass> getGymClasses() {
        GymCompany gymCompany = FileOperation.loadGymCompany();
        if(gymCompany != null) {
            return gymCompany.getClasses();
        }
        return null;
    }

    public static GymClass getGymClass(int id) {
        GymCompany gymCompany = FileOperation.loadGymCompany();
        if(gymCompany != null) {
            return gymCompany.findClass(id);
        }
        return null;
    }

    public static Schedule getGymClassSchedule(int id) {
        GymCompany gymCompany = FileOperation.loadGymCompany();
        if(gymCompany != null) {
            GymClass gymClass = gymCompany.findClass(id);
            if(gymClass != null){
                return gymClass.getSchedule();
            }
        }
        return null;
    }

    public static ArrayList<GymClass> getClassesUserIsAttending(int id) {
        GymCompany gymCompany = FileOperation.loadGymCompany();
        if(gymCompany != null){
            User user = gymCompany.findUser(id);
            if(user != null) {
                return gymCompany.getClassesThatUserIsAttending(user);
            }
        }
        return null;
    }

    public static boolean addGymClass(GymClass gymClass) {
        GymCompany gymCompany = FileOperation.loadGymCompany();
        gymCompany.addGymClass(gymClass);
        FileOperation.storeGymCompany(gymCompany);
        return true;
    }

    public static boolean updateGymClass(int id, GymClass g) {
        boolean ret = false;
        GymCompany gymCompany = FileOperation.loadGymCompany();
        GymClass gymClass = gymCompany.updateGymClass(id, g);
        if(gymClass != null) {
            FileOperation.storeGymCompany(gymCompany);
            ret = true;
        }
        return ret;
    }

    public static boolean deleteGymClass(int id) {
        boolean ret = false;
        GymCompany gymCompany = FileOperation.loadGymCompany();
        GymClass gymClass = gymCompany.deleteGymClass(id);
        if(gymClass != null) {
            FileOperation.storeGymCompany(gymCompany);
            ret = true;
        }
        return ret;
    }

    public static boolean isAttendingAClass(int id) {

        boolean ret = false;

        User user = Adapter.getUser(id);

        if(user != null){
            GymCompany gymCompany = FileOperation.loadGymCompany();

            if(gymCompany.isAttendingAClass(user)){
                ret = true;
            }
        }

        return ret;
    }

}
