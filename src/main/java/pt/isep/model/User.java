package pt.isep.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "user")
@XmlType(propOrder = { "id", "name", "accessCode"})
public class User implements Serializable {

    /**
     * Unique Identifier
     */
    private int id;

    private String name;

    private String accessCode;

    private static int counter = 1;

    public User(){
        this.id = counter;
        counter++;
    }

    public User(String name, String accessCode) {
        this.name = name;
        this.accessCode = accessCode;
        this.id = counter;
        counter++;
    }

    public int getId() {
        return id;
    }

    @XmlElement(name = "id")
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public String getAccessCode() {
        return accessCode;
    }

    @XmlElement(name = "accessCode")
    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public static User getUserInstance(User u){

        if(u == null) {
            return null;
        }
        if(u.name == null) {
            return null;
        }
        if(u.name.length() == 0){
            return null;
        }
        if(u.accessCode == null) {
            return null;
        }
        if(u.accessCode.length() == 0) {
            return null;
        }

        User user = new User();

        user.setName(u.name);
        user.setAccessCode(u.accessCode);

        return user;
    }
}
