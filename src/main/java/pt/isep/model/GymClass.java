package pt.isep.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;

@XmlRootElement(name = "gymClass")
@XmlType(propOrder = { "id","name", "schedule", "attendees"})
public class GymClass implements Serializable {

    private int id;

    private String name;

    private Schedule schedule;

    private ArrayList<User> attendees;

    private static int counter = 1;

    public GymClass(){
        this.id = counter;
        this.attendees = new ArrayList<User>();
        counter++;
    }

    public GymClass(String name, Schedule schedule) {
        this.name = name;
        this.schedule = schedule;
        this.id = counter;
        this.attendees = new ArrayList<User>();
        counter++;
    }

    public static GymClass getGymClassInstance(GymClass g) {

        if(g == null){
            return null;
        }

        if(g.name == null){
            return null;
        }

        if(g.name.trim().length() == 0){
            return null;
        }

        if(g.attendees == null){
            return null;
        }

        GymClass gymClass = new GymClass();
        gymClass.setName(g.name);
        gymClass.setSchedule(g.schedule);

        for(User attendee : g.attendees){
            gymClass.addAttendee(attendee);
        }

        return gymClass;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    @XmlElement(name = "schedule")
    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    @XmlElement(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<User> getAttendees() {
        return attendees;
    }

    @XmlElementWrapper(name = "attendees")
    @XmlElement(name = "attendee")
    public void setAttendees(ArrayList<User> attendees) {
        this.attendees = attendees;
    }

    public boolean addAttendee(User attendee){
        return attendees.add(attendee);
    }

    public boolean isAttending(User user){
        for(User attendee : attendees){
            if(attendee.getId() == user.getId()){
                return  true;
            }
        }

        return false;
    }
}
