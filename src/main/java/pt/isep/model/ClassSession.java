package pt.isep.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "session")
@XmlType(propOrder = { "startDate", "endDate", "weekDay"})
public class ClassSession implements Serializable {

    public enum WeekDay{
        MONDAY, TUESDAY, WEDNESDAY
        , THURSDAY, FRIDAY, SATURDAY
        , SUNDAY
    }

    private int startDate;

    private int endDate;

    private WeekDay weekDay;

    public ClassSession(){

    }

    public ClassSession(int startDate, int endDate, WeekDay weekDay) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.weekDay = weekDay;
    }

    public int getStartDate() {
        return startDate;
    }

    @XmlElement(name = "startDate")
    public void setStartDate(int startDate) {
        this.startDate = startDate;
    }

    public int getEndDate() {
        return endDate;
    }

    @XmlElement(name = "endDate")
    public void setEndDate(int endDate) {
        this.endDate = endDate;
    }

    public WeekDay getWeekDay() {
        return weekDay;
    }

    @XmlElement(name = "weekDay")
    public void setWeekDay(WeekDay weekDay) {
        this.weekDay = weekDay;
    }

}
