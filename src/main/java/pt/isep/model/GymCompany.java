package pt.isep.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;

@XmlRootElement(name = "gym")
@XmlType(propOrder = { "users", "classes"})
public class GymCompany implements Serializable {

    private ArrayList<User> users;

    private ArrayList<GymClass> classes;

    public GymCompany() {
        this.users = new ArrayList<User>();
        this.classes = new ArrayList<GymClass>();
    }

    @XmlElementWrapper(name = "users")
    @XmlElement(name = "user")
    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    @XmlElementWrapper(name = "classes")
    @XmlElement(name = "class")
    public void setClasses(ArrayList<GymClass> classes) {
        this.classes = classes;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public ArrayList<GymClass> getClasses() {
        return classes;
    }

    public boolean addUser(User user){
        return users.add(user);
    }

    public boolean addGymClass(GymClass gymClass){
        return classes.add(gymClass);
    }

    public User findUser(int id) {

        for(User user : users){
            if(user.getId() == id){
                return user;
            }
        }

        return null;
    }

    public User updateUser(int id, User u) {
        for(int i = 0; i< this.users.size();i++){
            if(this.users.get(i).getId() == id){
                u.setId(id);
                this.users.set(i,u);
                return this.users.get(i);
            }
        }
        return null;
    }

    public User deleteUser(int id) {
        for(int i = 0; i< this.users.size();i++){
            if(this.users.get(i).getId()==id){
                return this.users.remove(i);
            }
        }
        return null;
    }

    public GymClass findClass(int id) {

        for(GymClass gymClass : classes){
            if(gymClass.getId() == id){
                return gymClass;
            }
        }

        return null;
    }

    public GymClass updateGymClass(int id, GymClass g) {
        for(int i = 0; i< this.classes.size();i++){
            if(this.classes.get(i).getId() == id){
                g.setId(id);
                this.classes.set(i,g);
                return this.classes.get(i);
            }
        }
        return null;
    }

    public GymClass deleteGymClass(int id) {
        for(int i = 0; i< this.classes.size();i++){
            if(this.classes.get(i).getId()==id){
                return this.classes.remove(i);
            }
        }
        return null;
    }

    public boolean isAttendingAClass(User user) {

        ArrayList<GymClass> classesThatUserIsAttending = getClassesThatUserIsAttending(user);

        if(classesThatUserIsAttending.isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    public ArrayList<GymClass> getClassesThatUserIsAttending(User user){

        ArrayList<GymClass> attendingClasses = new ArrayList<GymClass>();

        for(GymClass gymClass : classes){

            if(gymClass.isAttending(user)){
                attendingClasses.add(gymClass);
            }
        }

        return attendingClasses;
    }
}
