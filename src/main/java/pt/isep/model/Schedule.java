package pt.isep.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;

@XmlRootElement(name = "schedule")
@XmlType(propOrder = { "classSessions"})
public class Schedule implements Serializable {

    private ArrayList<ClassSession> classSessions;

    public Schedule() {
        this.classSessions = new ArrayList<ClassSession>();
    }

    @XmlElementWrapper(name = "sessions")
    @XmlElement(name = "session")
    public void setClassSessions(ArrayList<ClassSession> classSessions) {
        this.classSessions = classSessions;
    }

    public ArrayList<ClassSession> getClassSessions() {
        return classSessions;
    }

    public boolean addClassSession(ClassSession session){
        return classSessions.add(session);
    }
}
