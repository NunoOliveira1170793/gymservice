package pt.isep.service;

import pt.isep.dao.Adapter;
import pt.isep.model.GymClass;
import pt.isep.model.GymCompany;
import pt.isep.model.Schedule;
import pt.isep.model.User;

import java.util.ArrayList;

/**
 * Os métodos retornam:
 * 1 - se a operação foi realizada
 * 0 - se o elemento não existe
 * -1 - se os dados estão corrompidos
 */
public class Utils {

    /*********************************************************/
    //Global
    public static GymCompany getGymCompany() {
        return Adapter.getGymCompany();
    }

    /*********************************************************/
    //Users

    public static ArrayList<User> getUsers() {
        return Adapter.getUsers();
    }

    public static User getUser(int id) {
        return Adapter.getUser(id);
    }

    public static int addUser(User u) {
        int ret = -1;
        User user = User.getUserInstance(u);
        if (user != null) {
            if (Adapter.addUser(user)) {
                ret = 1;
            }
        }
        return ret;
    }

    public static int updateUser(int id, User u) {
        int ret = 0;
        User user = User.getUserInstance(u);
        if (user != null) {
            if (Adapter.updateUser(id, user)) {
                ret = 1;
            }
        } else {
            ret = -1;
        }

        return ret;
    }

    public static int deleteUser(int id) {

        if(!Adapter.isAttendingAClass(id)) {

            if (Adapter.deleteUser(id)) {
                return 1;
            } else {
                return 0;
            }
        }else{
            return -1;
        }
    }

    /*********************************************************/
    //GymClasses

    public static ArrayList<GymClass> getGymClasses() {
        return Adapter.getGymClasses();
    }

    public static GymClass getGymClass(int id) {
        return Adapter.getGymClass(id);
    }

    public static Schedule getGymClassSchedule(int id) {
        return Adapter.getGymClassSchedule(id);
    }

    public static ArrayList<GymClass> getClassesUserIsAttending(int id) {
        return Adapter.getClassesUserIsAttending(id);
    }

    public static int addGymClass(GymClass g) {
        int ret = -1;
        GymClass gymClass = GymClass.getGymClassInstance(g);
        if (gymClass != null) {
            if (Adapter.addGymClass(gymClass)) {
                ret = 1;
            }
        }
        return ret;
    }

    public static int updateGymClass(int id, GymClass g) {
        int ret = 0;
        GymClass gymClass = GymClass.getGymClassInstance(g);
        if (gymClass != null) {
            if (Adapter.updateGymClass(id, gymClass)) {
                ret = 1;
            }
        } else {
            ret = -1;
        }

        return ret;
    }

    public static int deleteGymClass(int id) {
        if (Adapter.deleteGymClass(id)) {
            return 1;
        } else {
            return 0;
        }
    }
}
