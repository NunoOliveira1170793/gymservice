package pt.isep.service;

import pt.isep.model.GymClass;
import pt.isep.model.GymCompany;
import pt.isep.model.Schedule;
import pt.isep.model.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.SchemaOutputResolver;
import java.io.IOException;
import java.util.ArrayList;

@Path("/api")
public class GymService {

    /*****************************************************************************************************/
    //Global

    @GET
    @Path("/schema")
    public Response getXMLSchema() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(GymCompany.class);
        SchemaOutputResolver sor = new MySchemaOutputResolver();
        try {
            jaxbContext.generateSchema(sor);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Response.status(Response.Status.OK).entity(".......").build();
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_XML)
    public Response getGymCompany(){
        GymCompany gymCompany = Utils.getGymCompany();
        if(gymCompany != null) {
            return Response.ok(gymCompany, MediaType.APPLICATION_XML).build();
        }
        return Response.status(Response.Status.NOT_FOUND).entity("No data").build();
    }

    /*****************************************************************************************************/
    //Users

    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_XML)
    public Response getUsers(){
        ArrayList<User> users = Utils.getUsers();
        if(users != null) {
            return Response.ok(users.toArray(new User[users.size()]), MediaType.APPLICATION_XML).build();
        }
        return Response.status(Response.Status.NOT_FOUND).entity("No data").build();
    }

    @GET
    @Path("/users/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Response getUser(@PathParam("id") int id){
        User user = Utils.getUser(id);
        if(user != null) {
            return Response.ok(user, MediaType.APPLICATION_XML).build();
        }
        return Response.status(Response.Status.NOT_FOUND).entity("No data").build();
    }

    @POST
    @Path("/users")
    @Consumes(MediaType.APPLICATION_XML)
    public Response addUser(User user) {
        int ret = Utils.addUser(user);
        if(ret == 1) {
            return Response.status(Response.Status.CREATED).build();
        }
        return Response.status(Response.Status.NOT_ACCEPTABLE).entity("User not acceptable").build();
    }

    @PUT
    @Path("/users/{id}")
    @Consumes(MediaType.APPLICATION_XML)
    public Response updateUser(@PathParam("id")int id, User user) {

        int ret = Utils.updateUser(id, user);
        if(ret == 1){
            return Response.status(Response.Status.OK).build();
        }else {
            if (ret == 0) {
                return Response.status(Response.Status.NOT_FOUND).entity("User not found for id: " + id).build();
            } else {
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("User not acceptable").build();
            }
        }
    }

    @DELETE
    @Path("/users/{id}")
    public Response deleteUser(@PathParam("id") int id) {
        int ret = Utils.deleteUser(id);
        if(ret == 1) {
            return Response.status(Response.Status.OK).build();
        }
        if(ret == 0){
            return Response.status(Response.Status.NOT_FOUND).entity("User not found for id: " + id).build();
        }
        return Response.status(Response.Status.CONFLICT).entity("User " + id + " cannot be deleted").build();
    }

    /*****************************************************************************************************/
    //GymClass

    @GET
    @Path("/classes")
    @Produces(MediaType.APPLICATION_XML)
    public Response getGymClasses(){
        ArrayList<GymClass> gymClasses = Utils.getGymClasses();
        if(gymClasses != null) {
            return Response.ok(gymClasses.toArray(new GymClass[gymClasses.size()]), MediaType.APPLICATION_XML).build();
        }
        return Response.status(Response.Status.NOT_FOUND).entity("No data").build();
    }

    @GET
    @Path("/classes/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Response getGymClass(@PathParam("id") int id){
        GymClass gymClass = Utils.getGymClass(id);
        if(gymClass != null) {
            return Response.ok(gymClass, MediaType.APPLICATION_XML).build();
        }
        return Response.status(Response.Status.NOT_FOUND).entity("No data").build();
    }

    @GET
    @Path("/classes/{id}/schedule")
    @Produces(MediaType.APPLICATION_XML)
    public Response getGymClassSchedule(@PathParam("id") int id){
        Schedule schedule = Utils.getGymClassSchedule(id);
        if(schedule != null) {
            return Response.ok(schedule, MediaType.APPLICATION_XML).build();
        }
        return Response.status(Response.Status.NOT_FOUND).entity("No data").build();
    }

    @GET
    @Path("/classes/attendee/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Response getClassesUserIsAttending(@PathParam("id") int id){
        ArrayList<GymClass> gymClasses = Utils.getClassesUserIsAttending(id);
        if(gymClasses != null) {
            return Response.ok(gymClasses.toArray(new GymClass[gymClasses.size()]), MediaType.APPLICATION_XML).build();
        }
        return Response.status(Response.Status.NOT_FOUND).entity("No data").build();
    }

    @POST
    @Path("/classes")
    @Consumes(MediaType.APPLICATION_XML)
    public Response addGymClass(GymClass gymClass) {
        int ret = Utils.addGymClass(gymClass);
        if(ret == 1) {
            return Response.status(Response.Status.CREATED).build();
        }
        return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Class not acceptable").build();
    }

    @PUT
    @Path("/classes/{id}")
    @Consumes(MediaType.APPLICATION_XML)
    public Response updateGymClass(@PathParam("id")int id, GymClass gymClass) {

        int ret = Utils.updateGymClass(id, gymClass);
        if(ret == 1){
            return Response.status(Response.Status.OK).build();
        }else {
            if (ret == 0) {
                return Response.status(Response.Status.NOT_FOUND).entity("Class not found for id: " + id).build();
            } else {
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Class not acceptable").build();
            }
        }
    }

    @DELETE
    @Path("/classes/{id}")
    public Response deleteGymClass(@PathParam("id") int id) {
        int ret = Utils.deleteGymClass(id);
        if(ret == 1) {
            return Response.status(Response.Status.OK).build();
        }
        if(ret == 0){
            return Response.status(Response.Status.NOT_FOUND).entity("Class not found for id: " + id).build();
        }
        return Response.status(Response.Status.CONFLICT).entity("Class " + id + " cannot be deleted").build();
    }
}
